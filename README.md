# FullStack Корпоративный портал COM.DESK
## Приложение COM.DESK поможет вам оставаться на связи и следить за успехами своих коллег внутри закрытого комьюнити
## Стэк - React, Redux, Express, MongoDB
### Видео презентация проекта: https://youtu.be/ZWpFWjCn5I8
### Команда разработчиков:
- [Александра М.](https://github.com/AleksandraMakhmutova)
- [Александр П.](https://github.com/PapakhinAV)
- [Мухтар А.](https://github.com/MuhtarAbdullaev)
- [Игорь В.](https://github.com/gavriil-volkov)
### Приложение возможно использовать как:
- решение Digital Signage для сенсорного киоска
- интранет внутри локальной сети
- социальная сеть
- закрытый портал
- блог
## Для запуска проекта
- Склонировать проект
- Инициализировать проект в корневой папке - yarn
- В папке backend инициализировать проект - npm i
- В корневой папке создать файл .env (Смотреть внизу README)
- В папке backend создать файл .env
- Запустить проект из папки backend - npm start
- Для запуска приложения используется база MongoDB в Atlas
- Приложение запустится после билда (дождаться окончания) по адресу localhost:3000
#
## Возможности приложения
### `Регистрация юзера`
Каждый юзер может зарегистрироваться указав свои данные.
![screenshot](gifs/Registration.gif)
#
### `Авторизация через социальные сети (Passport.JS)`
- GitHub - Автоматически подтягивается имя пользователи и аккаунт GitHub в профиль юзера
- Google - Автоматически подтягивается имя пользователи Google в профиль юзера
*Есть возможность добавить дополнительные сети для авторизации и подтягивать любую открытую информацию в профиль юзера
![screenshot](gifs/auth.gif)
#
### `Редактирование профиля юзера`
- Добавление/удаление фото
- Добавление/удаление группы
- Редактрирование контактных данных
- Редактирование социальных сетей
![screenshot](gifs/EditProfile.gif)
#
### `Добавление/удаление постов на странице юзера`
Каждый юзер может добавлять на своей страницы информационные посты. Пост может удалить только юзер разместивший его.
![screenshot](gifs/Add-and-Delete-Post.gif)
#
### `Парсер тех.новостей`
Парсер подтягивает 15 свежих и актуальных тех. новостей с новстного ресурсаs.
![screenshot](gifs/TechNews.gif)
#
### `Группы`
Есть возможность распределять юзеров по группам. Выбрать группу или выйти из нее можно в разделе "Редактировать профиль". В группе можно посмотреть всех юзеров которые вступили в нее. На странице юзера нельзя удалить его личные посты и редактировать его данные.
![screenshot](gifs/Groups.gif)
#
### `Отметка юзера на карте по ip (Яндекс API)`
Каждый юзер может отметить себя на карте. Отметка производится по текущему ip адресу.
![screenshot](gifs/CheckIn.gif)
#
### `Администратор`
Администратор может удалять юзеров, создавать/редактировать/удалять группы.
Login: admin@mail.ru
Password: aaaa
![screenshot](gifs/Admin.gif)
#
## Файл .env (Корневая папка):
SKIP_PREFLIGHT_CHECK=true
REACT_APP_URL=http://localhost:3000
#
## Файл .env (Папка backend):
PORT=3000
MONGO_DB='ЗАПРОСИТЬ У АВТОРА'
HOST:http://localhost:3001
SECRET:'ЗАПРОСИТЬ У АВТОРА'
GITHUB_CLIENT_ID = ЗАПРОСИТЬ У АВТОРА
GITHUB_CLIENT_SECRET = ЗАПРОСИТЬ У АВТОРА
GITHUB_CALLBACK_URL = /auth/github/callback
GOOGLE_CLIENT_ID = ЗАПРОСИТЬ У АВТОРА
GOOGLE_CLIENT_SECRET = ЗАПРОСИТЬ У АВТОРА
GOOGLE_CALLBACK_URL = /auth/google/callback
[
	{
		"name": "Желудки утиные сушено-вяленые",
		"price": 550,
		"discount": 15,
		"wight": "100 г",
		"description": "Описание demo",
		"isFavorite": false,
		"isCart": false,
		"available": true,
		"stock": 10,
		"picture": "https://react-learning.ru/image-compressed/1.jpg"
	},
	{
		"name": "Куриные желудочки для собак",
		"price": 450,
		"discount": 10,
		"wight": "100 г",
		"description": "Описание demo",
		"isFavorite": false,
		"isCart": false,
		"available": true,
		"stock": 10,
		"picture": "https://react-learning.ru/image-compressed/2.jpg"
	},
	{
		"name": "Крупная говяжья сушено-вяленая жилка",
		"price": 360,
		"discount": 0,
		"wight": "100 г",
		"description": "Описание demo",
		"isFavorite": false,
		"isCart": false,
		"available": true,
		"stock": 10,
		"picture": "https://react-learning.ru/image-compressed/3.jpg"
	},
	{
		"name": "Мелкая говяжья сушено-вяленая жилка",
		"price": 300,
		"discount": 0,
		"wight": "100 г",
		"description": "Описание demo",
		"isFavorite": false,
		"isCart": false,
		"available": true,
		"stock": 10,
		"picture": "https://react-learning.ru/image-compressed/4.jpg"
	},
	{
		"name": "Калтык говяжий для собак",
		"price": 290,
		"discount": 0,
		"wight": "100 г",
		"description": "Описание demo",
		"isFavorite": false,
		"isCart": false,
		"available": true,
		"stock": 10,
		"picture": "https://react-learning.ru/image-compressed/5.jpg"
	},
	{
		"name": "Бублик из бычьего корня",
		"price": 340,
		"discount": 0,
		"wight": "100 г",
		"description": "Описание demo",
		"isFavorite": false,
		"isCart": false,
		"available": true,
		"stock": 10,
		"picture": "https://react-learning.ru/image-compressed/6.jpg"
	},
	{
		"name": "Копыто оленье",
		"price": 490,
		"discount": 0,
		"wight": "1 шт",
		"description": "Описание demo",
		"isFavorite": false,
		"isCart": false,
		"available": true,
		"stock": 10,
		"picture": "https://react-learning.ru/image-compressed/7.jpg"
	},
	{
		"name": "Бараний рубец сушенный",
		"price": 330,
		"discount": 0,
		"wight": "100 г",
		"description": "Описание demo",
		"isFavorite": false,
		"isCart": false,
		"available": true,
		"stock": 10,
		"picture": "https://react-learning.ru/image-compressed/8.jpg"
	},
	{
		"name": "Рубец говяжий для собак",
		"price": 290,
		"discount": 0,
		"wight": "100 г",
		"description": "Описание demo",
		"isFavorite": false,
		"isCart": false,
		"available": true,
		"stock": 10,
		"picture": "https://react-learning.ru/image-compressed/9.jpg"
	},
	{
		"name": "Свиные уши для собак",
		"price": 330,
		"discount": 0,
		"wight": "100 г",
		"description": "Описание demo",
		"isFavorite": false,
		"isCart": false,
		"available": true,
		"stock": 10,
		"picture": "https://react-learning.ru/image-compressed/10.jpg"
	},
	{
		"name": "Уши говяжьи для собак",
		"price": 330,
		"discount": 0,
		"wight": "100 г",
		"description": "Описание demo",
		"isFavorite": false,
		"isCart": false,
		"available": true,
		"stock": 10,
		"picture": "https://react-learning.ru/image-compressed/11.jpg"
	},
	{
		"name": "Печенья с яблоком (печень говяжья, сердце)",
		"price": 340,
		"discount": 0,
		"wight": "14 шт",
		"description": "Описание demo",
		"isFavorite": false,
		"isCart": false,
		"available": true,
		"stock": 10,
		"picture": "https://react-learning.ru/image-compressed/12.jpg"
	},
	{
		"name": "Рога оленя для собак весом до 3кг. Размер XS",
		"price": 350,
		"discount": 14,
		"wight": "1 шт",
		"description": "Описание demo",
		"isFavorite": false,
		"isCart": false,
		"available": true,
		"stock": 10,
		"picture": "https://react-learning.ru/image-compressed/13.jpg"
	},
	{
		"name": "Рога оленя для собак весом от 3 до 5 кг. Размер S",
		"price": 400,
		"discount": 0,
		"wight": "1 шт",
		"description": "Описание demo",
		"isFavorite": false,
		"isCart": false,
		"available": true,
		"stock": 10,
		"picture": "https://react-learning.ru/image-compressed/14.jpg"
	},
	{
		"name": "Рога оленя для собак весом от 5 до 10 кг. Размер M",
		"price": 600,
		"discount": 0,
		"wight": "1 шт",
		"description": "Описание demo",
		"isFavorite": false,
		"isCart": false,
		"available": true,
		"stock": 10,
		"picture": "https://react-learning.ru/image-compressed/15.jpg"
	},
	{
		"name": "Рога оленя для собак весом от 10 до 30 кг. Размер L",
		"price": 800,
		"discount": 0,
		"wight": "1 шт",
		"description": "Описание demo",
		"isFavorite": false,
		"isCart": false,
		"available": true,
		"stock": 10,
		"picture": "https://react-learning.ru/image-compressed/16.jpg"
	}
]
